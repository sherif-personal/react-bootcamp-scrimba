import React, {} from 'react';
import DefaultPropsTypes from './components/DefaultPropsTypes';
import ChildrenComponent from './components/ChildrenComponent';
import NavMenu from './components/NavMenu';
import DataFetcher from './components/DataFetcher';
import ReactMemo from './components/ReactMemo';

function App() {
  return (
    <>
      <h2>The React Bootcamp Scrimba</h2>
      <p>By Bob Ziroll</p>
      <NavMenu />
      <hr />
      <DefaultPropsTypes src="https://picsum.photos/id/237/300/300" borderRadius="10px" />
      <ChildrenComponent>
        <p>Component with child elements!</p>
      </ChildrenComponent>
      <hr />
      <DataFetcher url="https://rickandmortyapi.com/api/character/1">
        <h3>DataFetcher Component</h3>
      </DataFetcher>
      <hr />
      <h3>React.Memo() concept testing</h3>
      <ReactMemo />
    </>
  );
}

export default App;
