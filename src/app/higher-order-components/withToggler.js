/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
// Higher Order Component is a function that takes a component as a parameter
// and returns a new component wrapping the given component
// and "supercharging" it by giving it some extra abilities
import React, { useState } from 'react';

const Toggler = (props) => {
  const { component: Component, defaultShow } = props;
  const [show, setShow] = useState(defaultShow);

  const toggleShow = () => {
    setShow((prevState) => !prevState);
  };

  return (
    <Component show={show} toggleShow={toggleShow} {...props} />
  );
};

const withToggler = (component, options) => function togglerFn(porps) {
  return (
    <Toggler component={component} {...porps} defaultShow={options.defaultShow} />
  );
};

export default withToggler;
