import React from 'react';
import ProtoTypes from 'prop-types';
import withToggler from '../higher-order-components/withToggler';

function NavMenu(props) {
  const { show, toggleShow } = props;
  return (
    <div>
      <button type="button" onClick={toggleShow}>
        {(show) ? 'Hide' : 'Show'}
        &nbsp; Menu
      </button>
      <nav style={{ display: (show) ? 'block' : 'none' }}>
        <ul>
          <li>Item 1</li>
          <li>Item 2</li>
          <li>Item 3</li>
          <li>Item 4</li>
        </ul>
      </nav>
    </div>
  );
}

NavMenu.propTypes = {
  show: ProtoTypes.bool.isRequired,
  toggleShow: ProtoTypes.func.isRequired,
};

export default withToggler(NavMenu, { defaultShow: false });
