import React from 'react';

const ReactMemoGrandChild = () => {
  console.log('[ ]   [ ]   [ ]   [👶🏻] rendered');
  return (
    <div>
      <p>I am a ReactMemo Grand Child Component</p>
    </div>
  );
};

export default ReactMemoGrandChild;
