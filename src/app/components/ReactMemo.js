import React, { useState } from 'react';
import ReactMemoChild from './ReactMemoChild';

const ReactMemo = () => {
  const [count, setCount] = useState(0);
  const increment = () => {
    setCount((prevState) => prevState + 1);
  };
  console.log('[👴🏼]   [ ]   [ ]   [ ] rendered');

  return (
    <div>
      <p>I am a ReactMemo GrandParent Component</p>
      <button type="button" onClick={increment}>
        Update State:
        {count}
      </button>
      <ReactMemoChild />
      <ReactMemoChild />
    </div>
  );
};

export default ReactMemo;
