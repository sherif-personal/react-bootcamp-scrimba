import React from 'react';

function ChildrenComponent({ children }) {
  return (
    <div>
      {children}
    </div>
  );
}


ChildrenComponent.defaultProps = {
  children: '',
};

export default ChildrenComponent;
