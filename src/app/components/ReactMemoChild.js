import React from 'react';
import ReactMemoGrandChild from './ReactMemoGrandChild';

const ReactMemoChild = () => {
  console.log('[ ]   [ ]   [🧒🏻]   [ ] rendered');
  return (
    <div>
      <p>I am a ReactMemo Child Component</p>
      <ReactMemoGrandChild />
      <ReactMemoGrandChild />
    </div>
  );
};

export default React.memo(ReactMemoChild);
