import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const DataFetcher = ({ children, ...props }) => {
  const { url } = props;
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    setLoading(true);
    fetch(url).then((res) => res.json()).then((resData) => {
      setLoading(false);
      setData(resData);
    });
  }, []);

  return (
    <div>
      { children}
      { (loading) ? '...Loading' : <p>{JSON.stringify(data)}</p>}
    </div>
  );
};

DataFetcher.propTypes = {
  url: PropTypes.string.isRequired,
};

export default DataFetcher;
