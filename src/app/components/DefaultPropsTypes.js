import React from 'react';
import Prototypes from 'prop-types';

function DefaultPropsTypes({ src, borderRadius }) {
  const style = {
    borderRadius,
  };

  return (
    <div className="Default-Props-Prototypes">
      <img className="round-img" src={src} alt="placeholder" style={style} />
    </div>
  );
}

DefaultPropsTypes.propTypes = {
  src: Prototypes.string.isRequired,
  borderRadius: Prototypes.oneOfType([
    Prototypes.string,
    Prototypes.number,
  ]),
};

DefaultPropsTypes.defaultProps = {
  borderRadius: '50%',
};

export default DefaultPropsTypes;
